libbytesize (2.9-1) unstable; urgency=medium

  * Team upload
  * New upstream version 2.9

 -- Michael Biebl <biebl@debian.org>  Sat, 08 Jul 2023 00:40:56 +0200

libbytesize (2.8-2) unstable; urgency=medium

  * Team upload
  * Use DEB_PYTHON_INSTALL_LAYOUT="deb" to get the correct Python library
    path.
    Instead of reverting back to distutils, set
    DEB_PYTHON_INSTALL_LAYOUT="deb" in debian/rules to get the correct
    sysconfig scheme on Debian.

 -- Michael Biebl <biebl@debian.org>  Thu, 29 Jun 2023 02:37:48 +0200

libbytesize (2.8-1) unstable; urgency=medium

  * Team upload
  * New upstream version 2.8
  * Use dh-sequence-python3 Build-Depends to enable the python3 addon
  * Bump Standards-Version to 4.6.2

 -- Michael Biebl <biebl@debian.org>  Tue, 13 Jun 2023 19:21:00 +0200

libbytesize (2.7-1) unstable; urgency=medium

  * Team upload

  [ Debian Janitor ]
  * Trim trailing whitespace.
  * Use canonical URL in Vcs-Browser.
  * Remove Section on libbytesize1, Section on libbytesize-common that
    duplicate source.

  [ Michael Biebl ]
  * New upstream version 2.7
  * Bump Standards-Version to 4.6.1
  * Drop no longer needed override for dh_missing.
    In compat 13 and later, --fail-missing is the default.
  * Use execute_before instead of override for dh_install
  * Revert "Do not use distutils to get Python library path"
    Otherwise the library path is not set properly with Python 3.10 on Debian.
  * Add a symbols file for libbytesize1
  * Tweak long package description a bit to please lintian

 -- Michael Biebl <biebl@debian.org>  Fri, 02 Sep 2022 12:53:58 +0200

libbytesize (2.6-1) unstable; urgency=medium

  * Team upload
  * New upstream version 2.6

 -- Michael Biebl <biebl@debian.org>  Mon, 16 Aug 2021 12:58:36 +0200

libbytesize (2.5-1) unstable; urgency=medium

  * Team upload
  * New upstream version 2.5
  * Bump Standards-Version to 4.5.1
  * Set upstream metadata fields: Bug-Submit, Bug-Database, Repository,
    Repository-Browse
  * Update Source URL in debian/copyright

 -- Michael Biebl <biebl@debian.org>  Sun, 07 Feb 2021 18:01:42 +0100

libbytesize (2.4-1) unstable; urgency=medium

  * Team upload
  * New upstream version 2.4
  * Bump debhelper-compat to 13

 -- Michael Biebl <biebl@debian.org>  Mon, 03 Aug 2020 09:16:12 +0200

libbytesize (2.3-2) unstable; urgency=medium

  * Team upload.
  * No change source-only upload

 -- Michael Biebl <biebl@debian.org>  Sun, 21 Jun 2020 00:37:09 +0200

libbytesize (2.3-1) unstable; urgency=medium

  * Team upload
  * New upstream version 2.3
  * Ship translations in a new package libbytesize-common

 -- Michael Biebl <biebl@debian.org>  Wed, 27 May 2020 17:07:21 +0200

libbytesize (2.2-1) unstable; urgency=medium

  * Team upload
  * New upstream version 2.2
  * Bump Standards-Version to 4.5.0

 -- Michael Biebl <biebl@debian.org>  Wed, 12 Feb 2020 15:49:31 +0100

libbytesize (2.1-1) unstable; urgency=medium

  * Team upload.
  * New upstream version 2.1
  * Drop obsolete --without-python2 configure flag.
    Python2 support has been removed upstream.
  * Disable tools via --withouth-tools configure switch
    bscalc is a simple bytesize calculator tool which was introduced in
    version 2.0. We don't really need it at this point so avoid the need for
    a new binary package by disabling it.
  * Fix formatting of package description
  * Mark libbytesize1 and libbytesize-dev as Multi-Arch: same
  * Switch from PCRE to PCRE2
  * Bump debhelper compat level to 12.
    Use new debhelper-compat syntax and drop debian/compat.

 -- Michael Biebl <biebl@debian.org>  Wed, 10 Jul 2019 12:41:07 +0200

libbytesize (1.4-1) unstable; urgency=medium

  * Team upload
  * New upstream version 1.4
  * Bump Standards-Version to 4.2.0
  * Explicitly enable Python3 support and disable Python2 support.
    We are only providing a python3 package and by not enabling Python2
    support in the first place, we don't have to manually remove any related
    files later on.

 -- Michael Biebl <biebl@debian.org>  Mon, 13 Aug 2018 18:56:45 +0200

libbytesize (1.3-1) unstable; urgency=medium

  * Team upload
  * New upstream version 1.3
  * Bump Standards-Version to 4.1.5

 -- Michael Biebl <biebl@debian.org>  Tue, 10 Jul 2018 22:28:27 +0200

libbytesize (1.2-3) unstable; urgency=medium

  * Team upload.
  * Add missing python3-six Depends to python3-bytesize (Closes: #896258)
  * Add missing debian/source/format using "3.0 (quilt)"

 -- Michael Biebl <biebl@debian.org>  Sat, 21 Apr 2018 08:51:33 +0200

libbytesize (1.2-2) unstable; urgency=medium

  * Update Vcs-* for the move to salsa.debian.org
  * Bump Standards-Version to 4.1.3. No changes necessary.

 -- Martin Pitt <mpitt@debian.org>  Sun, 25 Mar 2018 23:37:59 +0200

libbytesize (1.2-1) unstable; urgency=medium

  * Team upload

  [ Andreas Henriksson ]
  * Remove myself from uploaders

  [ Michael Biebl ]
  * Fix watch file
  * New upstream version 1.2
  * Use dh_missing --fail-missing instead of dh_install --fail-missing
  * Update Homepage URL
  * Bump Standards-Version to 4.1.1

 -- Michael Biebl <biebl@debian.org>  Wed, 01 Nov 2017 12:29:44 +0100

libbytesize (0.10-1) unstable; urgency=medium

  * Initial release. (Closes: #864815)

 -- Martin Pitt <mpitt@debian.org>  Sun, 18 Jun 2017 21:42:55 +0200
